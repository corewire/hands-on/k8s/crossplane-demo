# Crossplane 

## What is crossplane? 

- CNCF project
- Framework for building cloud native control planes
  - No need to write your own control plane
  - No need to write any code
  - Just write a declarative configuration
- Cloud Providers have been building control planes for years
  - AWS, Azure, GCP, etc.
  - When you spawn a VM, you are using the control plane of the cloud provider that orchestrates the VM
  - Crossplane can be used to build your own control plane - fitted to your needs
- X as Code excellence:
  - These guys took this serious and literally make it possible to manage everything in kubernetes, without writing your own operators or controllers.
  - They also offer a way to convert any terraform provider into a crossplane provider.
  - That's good, because writing controllers/providers is hard and time consuming and there are already a lot of terraform providers out there.
  - You can now use all of them in kubernetes, without writing a single line of code.
  - That also enables powerful GitOps workflows for everything.
- Adoption is surging.

## The Basics 
- Basically everything with an API can be managed by crossplane
``` 
---
# Example 1: Basic Role in Keycloak for Crossplane
# This is the simplest form of a role. It only specifies the realm and the name of the role.
apiVersion: role.keycloak.crossplane.io/v1alpha1
kind: Role
metadata:
  name: basic-role  # The name of the role in Kubernetes
spec:
  forProvider:
    realmId: "example-realm"  # The ID of the realm where this role will be created
    name: "basic-role"  # The name of the role in Keycloak
  providerConfigRef:
    name: "keycloak-provider-config"  # Reference to the ProviderConfig resource
```

- The Role resource is a Kubernetes Custom Resource (CR) and can be managed like any other Kubernetes resource.
- The Role resource contains the specification of the role that should be created in Keycloak
- This is a declarative approach and defines the desired state. Crossplane will then make sure that the actual state matches the desired state.
- Crossplane uses the concept of a ProviderConfig to connect to the API of the provider
- The ProviderConfig is a Kubernetes resource that contains the credentials to connect to the API of the provider
- The ProviderConfig is referenced by the Role resource
- Applying the Role resource will create the role in Keycloak:
```
$ kubectl apply -f role.yaml
role.keycloak.crossplane.io/basic-role created
```
- The role is now created in Keycloak
- The role can be deleted by deleting the Role resource:
```
$ kubectl delete -f role.yaml
role.keycloak.crossplane.io "basic-role" deleted
```
- The role is now deleted in Keycloak

## Compositions
- Compositions are a powerful feature of Crossplane
- Build your own Platform API. 

### XRDs 
- Crossplane Resource Definitions (XRDs) are similar to Kubernetes Custom Resource Definitions (CRDs)
- Creating a CompositeResourceDefinition consists of:
  - Defining a custom API group.
  - Defining a custom API name.
  - Defining a custom API schema and version.
- Optionally, CompositeResourceDefinitions also support:
  - Offering a Claim.
  - Defining connection secrets.
  - Setting composite resource defaults.

### Basic XRD Example
```

```






