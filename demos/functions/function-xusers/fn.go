package main

import (
	"context"

	"github.com/crossplane/crossplane-runtime/pkg/errors"
	"github.com/crossplane/crossplane-runtime/pkg/logging"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/ptr"

	crossv1 "github.com/crossplane/crossplane-runtime/apis/common/v1"
	fnv1beta1 "github.com/crossplane/function-sdk-go/proto/v1beta1"
	"github.com/crossplane/function-sdk-go/request"
	"github.com/crossplane/function-sdk-go/resource"
	"github.com/crossplane/function-sdk-go/resource/composed"
	"github.com/crossplane/function-sdk-go/response"

	"github.com/crossplane-contrib/provider-keycloak/apis/user/v1alpha1"
)

// Function returns whatever response you ask it to.
type Function struct {
	fnv1beta1.UnimplementedFunctionRunnerServiceServer

	log logging.Logger
}

// RunFunction runs the Function.
func (f *Function) RunFunction(_ context.Context, req *fnv1beta1.RunFunctionRequest) (*fnv1beta1.RunFunctionResponse, error) {
	f.log.Info("Running Function", "tag", req.GetMeta().GetTag())

	// Create a response to the request. This copies the desired state and
	// pipeline context from the request to the response.
	rsp := response.To(req, response.DefaultTTL)

	// Read the observed XR from the request. Most functions use the observed XR
	// to add desired managed resources.
	xr, err := request.GetObservedCompositeResource(req)
	if err != nil {
		// If the function can't read the XR, the request is malformed. This
		// should never happen. The function returns a fatal result. This tells
		// Crossplane to stop running functions and return an error.
		response.Fatal(rsp, errors.Wrapf(err, "cannot get observed composite resource from %T", req))
		return rsp, nil
	}

	// Create an updated logger with useful information about the XR.
	log := f.log.WithValues(
		"xr-version", xr.Resource.GetAPIVersion(),
		"xr-kind", xr.Resource.GetKind(),
		"xr-name", xr.Resource.GetName(),
	)

	users, err := xr.Resource.GetStringArray("spec.users")
	if err != nil {
		response.Fatal(rsp, errors.Wrapf(err, "cannot read spec.users field of %s", xr.Resource.GetKind()))
		return rsp, nil
	}

	// Get all desired composed resources from the request. The function will
	// update this map of resources, then save it. This get, update, set pattern
	// ensures the function keeps any resources added by other functions.
	desired, err := request.GetDesiredComposedResources(req)
	if err != nil {
		response.Fatal(rsp, errors.Wrapf(err, "cannot get desired resources from %T", req))
		return rsp, nil
	}

	_ = v1alpha1.AddToScheme(composed.Scheme)

	// Create users
	for _, user := range users {
		f.log.Info("User", "name", user)
		u := &v1alpha1.User{
			ObjectMeta: metav1.ObjectMeta{
				Name: user,
			},
			Spec: v1alpha1.UserSpec{
				ForProvider: v1alpha1.UserParameters{
					Username: ptr.To[string](user),
					RealmID:  ptr.To[string]("master"),
					InitialPassword: []v1alpha1.InitialPasswordParameters{
						{
							Temporary: ptr.To[bool](true),
							ValueSecretRef: crossv1.SecretKeySelector{
								Key: "password",
								SecretReference: crossv1.SecretReference{
									Name:      "initial-password-secret",
									Namespace: "iam",
								},
							},
						},
					},
				},
			},
		}

		// Convert the bucket to the unstructured resource data format the SDK
		// uses to store desired composed resources.
		cd, err := composed.From(u)
		if err != nil {
			response.Fatal(rsp, errors.Wrapf(err, "cannot convert %T to %T", u, &composed.Unstructured{}))
			return rsp, nil
		}

		// Add the bucket to the map of desired composed resources. It's
		// important that the function adds the same bucket every time it's
		// called. It's also important that the bucket is added with the same
		// resource.Name every time it's called. The function prefixes the name
		// with "xuser-" to avoid collisions with any other composed
		// resources that might be in the desired resources map.
		desired[resource.Name("xuser-"+user)] = &resource.DesiredComposed{Resource: cd}
	}

	// Finally, save the updated desired composed resources to the response.
	if err := response.SetDesiredComposedResources(rsp, desired); err != nil {
		response.Fatal(rsp, errors.Wrapf(err, "cannot set desired composed resources in %T", rsp))
		return rsp, nil
	}

	// Log what the function did. This will only appear in the function's pod
	// logs. A function can use response.Normal and response.Warning to emit
	// Kubernetes events associated with the XR it's operating on.
	log.Info("Added desired users", "users", users, "count", len(users))

	return rsp, nil
}
