"""A Crossplane composition function."""

import grpc
from typing import List
from crossplane.function import logging, response
from crossplane.function.proto.v1beta1 import run_function_pb2 as fnv1beta1
from crossplane.function.proto.v1beta1 import run_function_pb2_grpc as grpcv1beta1
from google.protobuf import json_format
from google.protobuf import struct_pb2

import json

class xUnitSpec:
    def __init__(self, name: str, users: List[str]):
        self.name = name
        self.users = users

class xUnitResource:
    def __init__(self, apiVersion: str, kind: str, metadata: dict, spec: xUnitSpec):
        self.apiVersion = apiVersion
        self.kind = kind
        self.metadata = metadata
        self.spec = spec

    @staticmethod
    def from_proto(resource_proto: fnv1beta1.Resource):
        resource_dict = json_format.MessageToDict(resource_proto.resource)
        spec = xUnitSpec(name=resource_dict['spec']['name'], 
                         users=resource_dict['spec']['users'])
        return xUnitResource(apiVersion=resource_dict['apiVersion'],
                             kind=resource_dict['kind'],
                             metadata=resource_dict['metadata'],
                             spec=spec)

class FunctionRunner(grpcv1beta1.FunctionRunnerService):
    """A FunctionRunner handles gRPC RunFunctionRequests."""

    def __init__(self):
        """Create a new FunctionRunner."""
        self.log = logging.get_logger()

    def extract_users(self,req):
        # Access the composite resource
        composite = req.observed.composite
        # Extract the 'spec' field
        spec = composite.resource.fields.get('spec')
        # Navigate to 'users' within 'spec'
        users_field = spec.struct_value.fields.get('users')
        # Extract the list of users
        users_list = [user.string_value for user in users_field.list_value.values]
        return users_list

    async def RunFunction(
        self, req: fnv1beta1.RunFunctionRequest, _: grpc.aio.ServicerContext
    ) -> fnv1beta1.RunFunctionResponse:
        """Run the function."""
        log = self.log.bind(tag=req.meta.tag)
        log.info("Running function")

        rsp = response.to(req)

        log.info(f"Received request: {req}")

        # Extract the users list from the request
        users = self.extract_users(req)
        log.info(f"Extraced Users: {users}")

        # Use class 
        composite_resource = xUnitResource.from_proto(req.observed.composite)
        users = composite_resource.spec.users
        log.info(f"Class Extraced Users: {users}")

        # Create a map of users to resources
        for user in users:
            user_resource = {
                "apiVersion": "user.keycloak.crossplane.io/v1alpha1",
                "kind": "User",
                "metadata": {
                    "name": user,
                },
                "spec": {
                    "forProvider": {
                        "username": user,
                        "realmId": "master"
                    },
                    "providerConfigRef": {
                        "name": "keycloak-provider-config"
                    }
                }
            }
            user_resource_struct = struct_pb2.Struct()
            json_format.ParseDict(user_resource, user_resource_struct)
            user_proto = fnv1beta1.Resource()
            user_proto.resource.CopyFrom(user_resource_struct)
            rsp.desired.resources[user].CopyFrom(user_proto)
        
        response.normal(rsp, f"I was run and output {rsp.desired}!")
        log.info("I was run!")
        return rsp
