# Compositions

Compositions are a template for creating multiple managed resources as a single object.
A Composition composes individual managed resources together into a larger, reusable, solution.
An example Composition may combine a virtual machine, storage resources and networking policies. 
A Composition template links all these individual resources together.

```
