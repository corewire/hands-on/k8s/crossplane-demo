import os
from keycloak import KeycloakAdmin


KEYCLOAK_IP = os.environ.get("KEYCLOAK_IP", None)   
KEYCLOAK_PORT = os.environ.get("KEYCLOAK_PORT", None)
KC_REALM = "master"
KC_SERVER_URL = f'http://{KEYCLOAK_IP}:{KEYCLOAK_PORT}/auth/'
KC_USER = "admin"
KC_PASS = "admin"
credentials = {"username": KC_USER, "password": KC_PASS}

GRAFANA_IP = os.environ.get("GRAFANA_IP", None)
GRAFANA_PORT = os.environ.get("GRAFANA_PORT", None)
grafana_url = f'http://{GRAFANA_IP}:{GRAFANA_PORT}'
grafana_redirect_uri = f'http://{GRAFANA_IP}/login/generic_oauth'

admin = KeycloakAdmin(
        **credentials,
        server_url=KC_SERVER_URL,
        realm_name=KC_REALM,
        )

stefan = admin.create_user({"email": "stefan@corewire.de",
                                       "username": "stefan",
                                       "enabled": True,
                                       "firstName": "Steee",
                                       "lastName": "faaaan",
                                        "emailVerified": True,
                    "credentials": [{"value": "stefan","type": "password",}]},exist_ok=True)

julian = admin.create_user({"email": "julian@corewire.de",
                                       "username": "julian",
                                       "enabled": True,
                                       "firstName": "juuuuu",
                                       "lastName": "liiian",
                                        "emailVerified": True,
                    "credentials": [{"value": "julian","type": "password",}]},exist_ok=True)

janosch = admin.create_user({"email": "janosch@corewire.de",
                                       "username": "janosch",
                                       "enabled": True,
                                       "firstName": "jaaaaa",
                                       "lastName": "noooosch",
                                        "emailVerified": True,
                    "credentials": [{"value": "janosch","type": "password",}]},exist_ok=True)
client_roles_mapper = { 
    "config": { "access.token.claim": "true", "claim.name": "grafana.roles", "id.token.claim": "true", "jsonType.label": "String", "multivalued": "true", "userinfo.token.claim": "true" }, 
    "name": "clientroles", 
    "protocol": "openid-connect", 
    "protocolMapper": "oidc-usermodel-client-role-mapper" }
grafana_client_id = admin.create_client({"clientId": "grafana", "enabled": True, "redirectUris": [grafana_redirect_uri], "directAccessGrantsEnabled": True, "protocolMappers": [client_roles_mapper] }, skip_exists=True)
grafana_client_secret = admin.get_client_secrets(grafana_client_id)["value"]

with open(".grafana_client_id", "w") as f: 
    f.write(grafana_client_id)
with open(".grafana_client_secret", "w") as f:
    f.write(grafana_client_secret)

